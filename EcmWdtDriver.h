/******************************************************************************
*
* @copyright (C) 2020 NEXTW TECHNOLOGY CO., LTD.. All rights reserved.
*
* Sample Watchdog Driver for ECM-XF
*
******************************************************************************/

#ifndef _ECMWDTDRIVER_H_
#define _ECMWDTDRIVER_H_
#include <stdint.h>
#include "EcmDriver.h"
/******************************************************************************
* WDT_TIMEOUT Definitions
******************************************************************************/
#define ECM_WTD_TIMEOUT_16		0	/* A timeout event occurs if the WDT count reaches 16. (Approximately 1.6ms) */
#define ECM_WTD_TIMEOUT_64		1	/* A timeout event occurs if the WDT count reaches 64. (Approximately 6.4ms) */
#define ECM_WTD_TIMEOUT_256		2	/* A timeout event occurs if the WDT count reaches 256. (Approximately 25.6ms) */
#define ECM_WTD_TIMEOUT_1024	3	/* A timeout event occurs if the WDT count reaches 1024. (Approximately 102.4ms) */
#define ECM_WTD_TIMEOUT_4096	4	/* A timeout event occurs if the WDT count reaches 4096. (Approximately 409.6ms) */
#define ECM_WTD_TIMEOUT_16384	5	/* A timeout event occurs if the WDT count reaches 16384. (Approximately 1.64s) */
#define ECM_WTD_TIMEOUT_65536	6	/* A timeout event occurs if the WDT count reaches 65536. (Approximately 6.55s) */
#define	ECM_WTD_TIMEOUT_262144	7	/* A timeout event occurs if the WDT count reaches 262144. (Approximately 26.2s) */

/******************************************************************************
* WDT_RST_DELAY Definitions
******************************************************************************/
#define	ECM_WTD_RST_DELAY_3		3	/* If a timeout event occurs, the WDT system resets CPU in 3 WDT clocks. (Approximately 0.3ms) */
#define ECM_WTD_RST_DELAY_18	2	/* If a timeout event occurs, the WDT system resets CPU in 18 WDT clocks. (Approximately 1.8ms) */
#define ECM_WTD_RST_DELAY_130	1	/* If a timeout event occurs, the WDT system resets CPU in 130 WDT clocks. (Approximately 13ms) */
#define ECM_WTD_RST_DELAY_1026	0	/* If a timeout event occurs, the WDT system resets CPU in 1026 WDT clocks. (Approximately 102.6ms) */

#ifdef __cplusplus
extern "C" {
#endif
/**
* Open and start WDT Timer
* The WDT function clock source is 10KHz.
* A timeout event will occur if the timeout is expired, and the WDT system will reset CPU after some delays.
*
* @param  timeout			: Watchdog timer timeout. See ECM_WTD_TIMEOUT_## definitions.
* @param  resetdelay		: Watchdog reset delay. See ECM_WTD_RST_DELAY_#### definitions.
*
* @return					: 1 for success; 0 for timeout; -1 for invalid input parameters.
*/
ECM_LIB_API int ECM_WdtOpen(uint32_t timeout, uint32_t resetdelay);

/**
* Open and start WDT Timer
* The WDT function clock source is 10KHz.
* A timeout event will occur if the timeout is expired, and the WDT system will reset CPU after some delays.
*
* @param  timeout			: Watchdog timer timeout. See ECM_WTD_TIMEOUT_## definitions.
* @param  resetdelay		: Watchdog reset delay. See ECM_WTD_RST_DELAY_#### definitions.
* @param  u8EnableCmdReset	: non-zero for enable SPI/USB command to reset WDT timer; 0 for disable.
*
* @return					: 1 for success; 0 for timeout; -1 for invalid input parameters.
*/
ECM_LIB_API int ECM_WdtOpenEx(uint32_t timeout, uint32_t resetdelay, uint8_t u8EnableCmdReset);

/**
* Close WDT Timer
*
* @return					: 1 for success; 0 for timeout; -1 for invalid input parameters.
*/
ECM_LIB_API int ECM_WdtClose(void);

/**
* Reset WDT Timer Counter
*
* @return					: 1 for success; 0 for timeout; -1 for invalid input parameters.
*/
ECM_LIB_API int ECM_WdtResetCounter(void);

/**
* Clear WDT Reset Flag
* See ECM_WdtGetResetFlag function.
*
* @return					: 1 for success; 0 for timeout; -1 for invalid input parameters.
*/
ECM_LIB_API int ECM_WdtClearResetFlag(void);

/**
* Get WDT Reset Flag
*
* @param  flag				: WDT reset flag pointer. If the system is reset by WDT, *(@flag) will be set to 1; otherwise, *(@flag) will be set to 0.
*
* @return					: 1 for success; 0 for timeout; -1 for invalid input parameters.
*/
ECM_LIB_API int ECM_WdtGetResetFlag(uint32_t *flag);

/**
* Clear WDT Timeout Flag
* See ECM_WdtGetTimeoutFlag function.
*
* @return					: 1 for success; 0 for timeout; -1 for invalid input parameters.
*/
ECM_LIB_API int ECM_WdtClearTimeoutFlag(void);

/**
* Get WDT Timeout Flag
*
* @param  flag				: WDT timeout flag pointer. If a timeout event occurs, *(@flag) will be set to 1; otherwise, *(@flag) will be set to 0.
*
* @return					: 1 for success; 0 for timeout; -1 for invalid input parameters.
*/
ECM_LIB_API int ECM_WdtGetTimeoutFlag(uint32_t *flag);

/**
 * Enable WDT Interrupt
 * @return					: 1 for success; 0 for timeout; -1 for invalid input parameters.
 */
ECM_LIB_API int ECM_WdtEnableInt(void);

/**
 * Disable WDT Interrupt
 * (For Ver 0x21 or later)
 * @return					: 1 for success; 0 for timeout; -1 for invalid input parameters.
 */
ECM_LIB_API int ECM_WdtDisableInt(void);

/**
 * Enable/disable SPI/USB command to reset WDT timer
 * (For Ver 0x21 or later)
 * @param  enable			: non-zero for enable; 0 for disable
 *
 * @return					: 1 for success; 0 for timeout; -1 for invalid input parameters.
 */
ECM_LIB_API int ECM_WdtCmdResetCounter(uint8_t enable);

#ifdef __cplusplus
}
#endif

#endif /* _ECMWDTDRIVER_H_ */
