/******************************************************************************
 *	File	:	EcmAdcDriver.cpp
 *	Version :	1.7
 *	Date	:	2022/07/27
 *	Author	:	XFORCE
 *
 *	ECM-XF ADC driver example
 *
 *	Demonstrate how to implement API type user driver
 *
 * @copyright (C) 2020 NEXTW TECHNOLOGY CO., LTD.. All rights reserved.
 *
 ******************************************************************************/
#include "stdafx.h"
#include "EcmUsrDriver.h"
#include "EcmAdcDriver.h"

extern CRITICAL_SECTION g_CriticalSection;
//extern SPI_CMD_PACKAGE_T *pCmd;
//extern SPI_RET_PACKAGE_T *pRet;

int ECM_AdcOpen( uint32_t mode)
{
	int i=0;
	uint8_t IdxCheck;
    if(mode > 1)
        return -1;
	EnterCriticalSection(&g_CriticalSection);
	g_SpiCmd.pHead->u8Cmd = ECM_ADC_FUNC_OP;
	g_SpiCmd.pHead->u8Param = 40;
	g_SpiCmd.pHead->u16Size = 4;
    g_SpiCmd.Data[0] = 0;
    g_SpiCmd.Data[1] = mode;
    g_SpiCmd.Data[2] = 0;
    g_SpiCmd.Data[3] = 0;
	g_SpiCmd.pHead->u8Idx++;
	for(i=0;i<100;i++){
		if(SpiDataExchange(&IdxCheck, 0)){
			if(g_SpiCmd.pHead->u8Idx == IdxCheck){
				LeaveCriticalSection(&g_CriticalSection);
				return 1;
			}
		}
	}
	LeaveCriticalSection(&g_CriticalSection);
	return 0;
}

int ECM_AdcClose(void)
{
	int i=0;
	uint8_t IdxCheck;
	EnterCriticalSection(&g_CriticalSection);
	g_SpiCmd.pHead->u8Cmd = ECM_ADC_FUNC_OP;
	g_SpiCmd.pHead->u8Param = 41;
	g_SpiCmd.pHead->u16Size = 0;
	g_SpiCmd.pHead->u8Idx++;
	for(i=0;i<100;i++){
		if(SpiDataExchange(&IdxCheck, 0)){
			if(g_SpiCmd.pHead->u8Idx == IdxCheck){
				LeaveCriticalSection(&g_CriticalSection);
				return 1;
			}
		}
	}
	LeaveCriticalSection(&g_CriticalSection);
	return 0;
}

int ECM_AdcConfigSampleModule( uint32_t source){
	int i;
	uint8_t IdxCheck;
	EnterCriticalSection(&g_CriticalSection);
	g_SpiCmd.pHead->u8Cmd = ECM_ADC_FUNC_OP;
	g_SpiCmd.pHead->u8Param = 42;
	g_SpiCmd.pHead->u16Size = 12;
    i = 12;
	memcpy( g_SpiCmd.Data, &i, sizeof(uint32_t));
	memcpy( g_SpiCmd.Data+4, &source, sizeof(uint32_t));
	memcpy( g_SpiCmd.Data+8, &i, sizeof(uint32_t));
	g_SpiCmd.pHead->u8Idx++;
	for(i=0;i<100;i++){
		if(SpiDataExchange(&IdxCheck, 0)){
			if(g_SpiCmd.pHead->u8Idx == IdxCheck){
				LeaveCriticalSection(&g_CriticalSection);
				return 1;
			}
		}
	}
	LeaveCriticalSection(&g_CriticalSection);
	return 0;
}

int ECM_AdcGetDataValidFlag(uint32_t *flag){
	uint32_t i;
	uint8_t IdxCheck;
	EnterCriticalSection(&g_CriticalSection);
	g_SpiCmd.pHead->u8Cmd = ECM_ADC_FUNC_OP;
	g_SpiCmd.pHead->u8Param = 17;
	g_SpiCmd.pHead->u16Size = 4;
	g_SpiCmd.pHead->u8Idx++;
	i = 1 << 12;
	memcpy( g_SpiCmd.Data, &i, sizeof(uint32_t));
	for(i=0;i<100;i++){
		if(SpiDataExchange(&IdxCheck, 0)){
			if(g_SpiCmd.pHead->u8Idx == IdxCheck){
				memcpy( flag, g_SpiRet.Data, sizeof(uint32_t));
				LeaveCriticalSection(&g_CriticalSection);
				return 1;
			}
		}
	}
	LeaveCriticalSection(&g_CriticalSection);
	return 0;
}

int ECM_AdcStartConv(void)
{
	uint32_t i=0;
	uint8_t IdxCheck;
	EnterCriticalSection(&g_CriticalSection);
	g_SpiCmd.pHead->u8Cmd = ECM_ADC_FUNC_OP;
	g_SpiCmd.pHead->u8Param = 12;
	g_SpiCmd.pHead->u16Size = 4;
	g_SpiCmd.pHead->u8Idx++;
	i = 1 << 12;
	memcpy( g_SpiCmd.Data, &i, sizeof(uint32_t));
	for(i=0;i<100;i++){
		if(SpiDataExchange(&IdxCheck, 0)){
			if(g_SpiCmd.pHead->u8Idx == IdxCheck){
				LeaveCriticalSection(&g_CriticalSection);
				return 1;
			}
		}
	}
	LeaveCriticalSection(&g_CriticalSection);
	return 0;
}

int ECM_AdcGetConvData( uint32_t *data){
	uint32_t i;
	uint8_t IdxCheck;
	EnterCriticalSection(&g_CriticalSection);
	g_SpiCmd.pHead->u8Cmd = ECM_ADC_FUNC_OP;
	g_SpiCmd.pHead->u8Param = 15;
	g_SpiCmd.pHead->u16Size = 4;
	g_SpiCmd.pHead->u8Idx++;
    i = 12;
	memcpy( g_SpiCmd.Data, &i, sizeof(uint32_t));
	for(i=0;i<100;i++){
		if(SpiDataExchange(&IdxCheck, 0)){
			if(g_SpiCmd.pHead->u8Idx == IdxCheck){
				memcpy( data, g_SpiRet.Data, sizeof(uint32_t));
				LeaveCriticalSection(&g_CriticalSection);
				return 1;
			}
		}
	}
	LeaveCriticalSection(&g_CriticalSection);
	return 0;
}
