/******************************************************************************
 *	File	:	EcmUsrDriver.h
 *	Version :	1.8
 *	Date	:	2025/02/03
 *	Author	:	XFORCE
 *
 *	ECM-XF basic driver example - Header file
 *
 *	Demonstrate how to implement API type user driver
 *
 * @copyright (C) 2020 NEXTW TECHNOLOGY CO., LTD.. All rights reserved.
 *
 ******************************************************************************/

#ifndef _ECM_USR_DRV_H_
#define _ECM_USR_DRV_H_
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "EcmDriver.h"
#ifdef __cplusplus
extern "C" {
#endif
	/*
	 *
	 * Example
	 * 24MHz
	 * 500 bytes data
	 * 32 bytes header
	 * (500+32)*8/24 = 177us
	 * 177us + 50us = 227us(SPI communication time)
	 *
	 * NOTE:
	 * if(SPI communication time > ECAT Cycle time)
	 * Send more than one PDO in one SPI datagram
	 *
	 * */

#define TEST_SPI_IDLE_TIME		150
#define TEST_SPI_CRC_TYPE		ECM_CRC_TYPE_NONE

#define CIA402_SW_STATE_MASK						0x6F
#define CIA402_SW_NOTREADYTOSWITCHON				0x00
#define CIA402_SW_SWITCHEDONDISABLED				0x40
#define CIA402_SW_READYTOSWITCHON					0x21
#define CIA402_SW_SWITCHEDON						0x23
#define CIA402_SW_OPERATIONENABLED					0x27
#define CIA402_SW_QUICKSTOPACTIVE					0x07
#define CIA402_SW_FAULTREACTIONACTIVE				0x0F
#define CIA402_SW_FAULT								0x08
#define ECMDRVVER 0x21010600
	ECM_LIB_API uint8_t *ECM_GetSpiTxBuf();
	ECM_LIB_API uint8_t *ECM_GetSpiRxBuf();
	ECM_LIB_API uint8_t * ECM_GetTxPdoBuf();
	ECM_LIB_API uint8_t * ECM_GetRxPdoBuf();
	ECM_LIB_API int32_t  EcmUniversalCmd(uint8_t u8Cmd, \
		uint8_t u8Param, \
		uint8_t u8Param0, \
		uint8_t u8Param1, \
		uint8_t u8Param2, \
		uint8_t u8Param3, \
		uint8_t *pu8Return, \
		uint8_t *pu8Status, \
		uint8_t *pu8ErrStatus, \
		uint16_t *pu16ReturnDataSize, \
		uint8_t *pu8ReturnData);
	ECM_LIB_API int32_t  ECM_WaitAsyncDone(int32_t nMS);
	ECM_LIB_API int32_t  ECM_IsAsyncBusy(void);
	ECM_LIB_API int32_t SpiDataExchange(uint8_t *RetIdx, uint8_t *RetCmd);
	ECM_LIB_API int32_t  ECM_GetFirmwareVer(uint8_t *pVersion, uint32_t *pExtVer);
	ECM_LIB_API int32_t  ECM_CheckDCStable(void);
	ECM_LIB_API int32_t  ECM_IsSlaveAlive(uint8_t *pEcmStatus, uint8_t *pRxPDOFifoCnt, uint8_t *CrcErrCnt, uint8_t *WkcErrCnt);
	ECM_LIB_API int32_t  ECM_InfoUpdate(uint8_t *pEcmStatus, uint8_t *pRxPDOFifoCnt, uint8_t *CrcErrCnt, uint8_t *WkcErrCnt);
	ECM_LIB_API int32_t  ECM_EcatInit(uint16_t DCActCode, uint32_t CycTime);
	ECM_LIB_API int32_t  ECM_GetRetStatus(uint8_t *pStatus);
	ECM_LIB_API int32_t  ECM_GetRetErrStatus(uint8_t *pErrStatus);
	ECM_LIB_API uint8_t  ECM_InitLibrary(uint16_t *pu16SpiDataSize, uint8_t u8CrcType);
	ECM_LIB_API void  ECM_CloseLibrary();
	ECM_LIB_API int32_t  ECM_EcatSetDCSync(uint8_t u8SlvIdx, uint16_t DCActCode, uint32_t CycTime);
	ECM_LIB_API int32_t  ECM_EcatReconfig();
	ECM_LIB_API int8_t  ECM_EcatSlvCntGet();
	ECM_LIB_API int32_t  ECM_EcatStateSet(uint8_t u8Slave, uint8_t u8State);
	ECM_LIB_API int32_t  ECM_EcatStateUpdate(void);
	ECM_LIB_API int32_t  ECM_EcatStateGet(uint8_t u8Slave, uint8_t *pu8State);
	ECM_LIB_API int32_t  ECM_EcatPdoConfigSet(uint8_t Slave, PDO_CONFIG_HEAD *pConfigData);
	ECM_LIB_API int32_t  ECM_EcatPdoConfigReq(uint8_t Slave, uint16_t SmaIdx);
	ECM_LIB_API int32_t  ECM_EcatPdoConfigGet(PDO_CONFIG_HEAD *pBuf);
	ECM_LIB_API int32_t  ECM_EcatSdoReq(uint8_t OP, uint8_t Slave, uint16_t Index, uint8_t SubIndex, uint16_t size, int32_t Timeout, uint8_t *Data);
	ECM_LIB_API int32_t  ECM_EcatSdoCAReq(uint8_t OP, uint8_t Slave, uint16_t Index, uint8_t SubIndex, uint16_t size, int32_t Timeout, uint8_t *Data);
	ECM_LIB_API int32_t  ECM_EcatSdoGet(uint8_t *pBuf);
	ECM_LIB_API int32_t  ECM_EcatSdoWrite(uint8_t Slave, uint16_t Index, uint8_t SubIndex, uint16_t size, int32_t Timeout, uint8_t *Data);
	ECM_LIB_API int32_t  ECM_EcatSdoRead(uint8_t Slave, uint16_t Index, uint8_t SubIndex, uint16_t size, int32_t Timeout, uint8_t *Data);
	ECM_LIB_API int32_t  ECM_Drv402SM_AdvConfig(uint8_t TblIdx, uint8_t SlvIdx, uint8_t ContrlWordOffset, uint8_t StateWordOffset);
	ECM_LIB_API int32_t  ECM_Drv402SM_Enable(uint8_t TblIdx, uint8_t SlvIdx);
	ECM_LIB_API int32_t  ECM_Drv402SM_Disable(uint8_t TblIdx, uint8_t SlvIdx);
	ECM_LIB_API int32_t  ECM_Drv402SM_StateSet(uint8_t TblIdx, uint8_t State);
	ECM_LIB_API int32_t  ECM_Drv402SM_StateGet(uint8_t TblIdx, uint8_t *pState);
	ECM_LIB_API int32_t  ECM_Drv402SM_StateCheck(uint8_t TblIdx, uint8_t ExceptState, int32_t TimeOutMS);
	ECM_LIB_API uint16_t  ECM_FifoRxPdoSizeGet();
	ECM_LIB_API uint16_t  ECM_FifoTxPdoSizeGet();

	ECM_LIB_API uint8_t  ECM_EcatPdoDataExchange(uint8_t u8OP, uint8_t *pRxData, uint8_t *pTxData, uint16_t *pu16DataSize);
	ECM_LIB_API int32_t  ECM_EcatPdoFifoDataExchange(uint8_t u8FifoThreshold, uint8_t *pRxData, uint8_t *pTxData, uint16_t u16DataSize, uint8_t *pRxPDOFifoCnt, uint8_t *CrcErrCnt, uint8_t *WkcErrCnt);
	ECM_LIB_API int32_t  ECM_EcatPdoFifoDataExchangeAdv(uint8_t u8Op, uint8_t u8Cnt, uint8_t *pRxData, uint8_t *pTxData, uint16_t u16DataSize, uint8_t *pu8RxPdoFifoCnt, uint8_t *CrcErrCnt, uint8_t *WkcErrCnt, uint8_t *IsSlvAlive);
	ECM_LIB_API int32_t  ECM_EcatPdoFifoIsFull(uint8_t u8FifoThreshold);
	ECM_LIB_API int32_t  ECM_EcatEepromReq(uint16_t OP, uint16_t slave, uint16_t eeproma, uint16_t data, uint32_t timeout);
	ECM_LIB_API int32_t  ECM_EcatEepromGet(uint64_t *pu64Data);
	ECM_LIB_API int32_t  ECM_GetPDOConfig(int32_t Slave, int32_t SmaIdx, PDO_CONFIG_HEAD *pPdoConfigBuf);
	//ECM_LIB_API int32_t  ECM_ShowPDOConfig(int32_t Slave, int32_t SmaIdx);
	ECM_LIB_API int32_t  ECM_ShowPDOConfig(int32_t Slave, int32_t SmaIdx);
	ECM_LIB_API int32_t  ECM_StateCheck(uint8_t u8Slave, uint8_t u8ExpectState, int32_t TimeOutMS);
	ECM_LIB_API int32_t  ECM_EcatDatagramReq(
		ecm_datagram_commad_t cmd,
		uint16_t position,
		uint16_t offset,
		uint32_t logicalAddress,
		uint16_t length,
		int32_t Timeout,
		uint8_t *Data);
	ECM_LIB_API int32_t  ECM_EcatDatagramGet(uint8_t *pBuf);
	ECM_LIB_API int32_t  ECM_SlaveInfoGet(uint8_t slave, uint8_t info, uint8_t *pBuf);

	ECM_LIB_API int32_t  ECM_SetTxFIFOCnt(uint8_t u8TxCnt);
	ECM_LIB_API int32_t  ECM_SetRxFIFOCnt(uint8_t u8RxCnt);
	ECM_LIB_API int32_t  ECM_GetTxFIFOCnt(uint8_t *pu8Cnt);
	ECM_LIB_API int32_t  ECM_GetRxFIFOCnt(uint8_t *pu8Cnt);

	ECM_LIB_API int32_t  ECM_ClearFIFO(uint8_t u8TxRx);
	ECM_LIB_API int32_t  ECM_EnableFIFO(uint8_t u8Enable);
	ECM_LIB_API int32_t  ECM_InitFIFO(void);
	ECM_LIB_API int ECM_ConfigFIFO(uint8_t u8Enable, uint8_t u8TxFIFOCnt, uint8_t u8RxFIFOCnt);
	ECM_LIB_API int32_t  ECM_SetSpiDataSize(uint16_t u16SpiDataSize);
	ECM_LIB_API int32_t  ECM_GetSpiDataSize(uint16_t *pu16SpiDataSize);

	ECM_LIB_API int32_t EcmSpiPackSizeCal();

	ECM_LIB_API int32_t  ECM_EcatRawCmdReq(uint16_t OP, \
		uint16_t length, \
		uint16_t ADP, \
		uint16_t ADO, \
		uint32_t LogAdr, \
		int32_t timeout, \
		uint8_t *data);
	ECM_LIB_API uint16_t  ECM_EcatRawCmdGet(uint8_t *pBuf);
	ECM_LIB_API uint16_t  ECM_EcatRawCmdRead(uint16_t OP, \
		uint16_t length, \
		uint16_t ADP, \
		uint16_t ADO, \
		uint32_t LogAdr, \
		int32_t timeout, \
		uint8_t *data);
	ECM_LIB_API int32_t  ECM_EcatWkcErrorMaxSet(uint8_t u8Max);
	ECM_LIB_API int32_t  ECM_EcatWkcErrorMaxGet(uint8_t *pu8Max);
	ECM_LIB_API int32_t  ECM_GetLogTime(uint8_t u8Timer, uint8_t u8Idx, uint8_t u8Type);
	ECM_LIB_API int32_t  ECM_ShowReturnField();
	ECM_LIB_API int32_t  ECM_ShowCmdField();
	//int32_t  ECM_EcatSdoSetPdoConfig(uint8_t Slave, PDO_CONFIG_HEAD *pConfigData);
	ECM_LIB_API int32_t  ECM_EcatSdoSetPdoConfig(PDO_CONFIG_HEAD *pConfigData);
	ECM_LIB_API int32_t  ECM_EcatConfigDC(void);
	ECM_LIB_API int32_t  ECM_EcatConfigMap(void);
	ECM_LIB_API int32_t  ECM_EcatConfigSM(uint8_t slave, uint8_t nSM, uint16_t StartAddr, uint16_t SMlength, uint8_t ControlReg, uint8_t Activate);
	ECM_LIB_API int32_t  ECM_EcatConfigFMMU(uint8_t slave, uint8_t FMMU0func, uint8_t FMMU1func, uint8_t FMMU2func, uint8_t FMMU3func);
	ECM_LIB_API int32_t  ECM_Reset();
	ECM_LIB_API int32_t  ECM_SkipNetInit();
	ECM_LIB_API int32_t  ECM_ResetPhy();
	ECM_LIB_API int32_t  ECM_ManualEmacInit();
	ECM_LIB_API int32_t  ECM_ForceClearAsyncBusy();
	/*
	 * u32CompIntEnable : Complex interrupt
	 * 		BIT31	:	EtherCAT package receive
	 * 		BIT25	:	RxFIFO Low threshold
	 *		BIT24	:	TxFIFO High threshold
	 *		BIT23	:	CRC	error
	 *		BIT22	:	EtherCAT working counter error
	 * u8GpioIntEnable : GPIO0~7 input interrupt
	 * 		BIT0	:	GPIO00 INT 		( marco : ECM_HEAD_INTFALG_GPIO00 )
	 * 		BIT1	:	GPIO01 INT 		( marco : ECM_HEAD_INTFALG_GPIO01 )
	 * 		BIT2	:	GPIO02 INT 		( marco : ECM_HEAD_INTFALG_GPIO02 )
	 * 		BIT3	:	GPIO03 INT 		( marco : ECM_HEAD_INTFALG_GPIO03 )
	 * 		BIT4	:	GPIO04 INT 		( marco : ECM_HEAD_INTFALG_GPIO04 )
	 * 		BIT5	:	GPIO05 INT 		( marco : ECM_HEAD_INTFALG_GPIO05 )
	 * 		BIT6	:	GPIO06 INT 		( marco : ECM_HEAD_INTFALG_GPIO06 )
	 * 		BIT7	:	GPIO07 INT 		( marco : ECM_HEAD_INTFALG_GPIO07 )
	 * u8PeripIntEnable : GPIO8~11 & QEI interrupt
	 * 		BIT0	:	GPIO08 INT 		( marco : ECM_HEAD_INTFALG_GPIO08 )
	 * 		BIT1	:	GPIO09 INT 		( marco : ECM_HEAD_INTFALG_GPIO09 )
	 * 		BIT2	:	GPIO10 INT 		( marco : ECM_HEAD_INTFALG_GPIO10 )
	 * 		BIT3	:	GPIO11 INT 		( marco : ECM_HEAD_INTFALG_GPIO11 )
	 * 		BIT4	:	QEI	index INT 	( marco : ECM_HEAD_INTFALG_QEI_IDX )
	 * 		BIT5	:	QEI	compare INT ( marco : ECM_HEAD_INTFALG_QEI_CMP )
	 * 		BIT6	:	QEI	Over/Under flow INT ( marco : ECM_HEAD_INTFALG_QEI_OUF )
	 * 		BIT7	:	QEI	direction change INT ( marco : ECM_HEAD_INTFALG_QEI_DIR )
	 * u8INTActiveHigh : INT0/INT1 interrupt signal polar
	 * 		BIT0	:	INT0 signal polar	(0 for Active low, others vice versa)
	 * 		BIT1	:	INT1 signal polar	(0 for Active low, others vice versa)
	 * ECM-XF Interrupt user note :
	 * 			Use the u32CompIntClr filed and u8GpioIntClr field of command header to CLEAR interrupt flags.
	 * 			Use the u32CompIntFlag field and u8GpioIntFlag field of command header to GET interrupt flags.
	 * */
	ECM_LIB_API int32_t  ECM_SetEcatIntEnable(uint32_t u32CompIntEnable, uint8_t u8GpioIntEnable, uint8_t u8PeripIntEnable, uint8_t u8INTActiveHigh);
	ECM_LIB_API int32_t  ECM_GetEcatIntEnable(uint32_t *pu32CompIntEnable, uint8_t *pu8GpioIntEnable, uint8_t *pu8PeripIntEnable, uint8_t *pu8INTActiveHigh);

	ECM_LIB_API int ECM_GetNetLinkPin(int *pPinStatus);
	ECM_LIB_API int32_t  ECM_Reset();
	/**/
	//ECM_LIB_API int32_t AX9_SetGpioDir(int32_t n32Idx, unsigned long ulDir);
	//ECM_LIB_API int32_t AX9_GetGpioDir(int32_t n32Idx, unsigned long *pulDir);
	ECM_LIB_API int32_t AX9_SetGpioDir(unsigned long ulDir);
	ECM_LIB_API int32_t AX9_GetGpioDir(unsigned long *pulDir);
	ECM_LIB_API int32_t AX9_SetGpioVal(unsigned long ulVal);
	ECM_LIB_API int32_t AX9_GetGpioVal(unsigned long *pulVal);
	ECM_LIB_API int32_t AX9_SetConfigPin(unsigned long ulVal);
	ECM_LIB_API int32_t AX9_GetConfigPin(unsigned long *pulVal);
	ECM_LIB_API int32_t AX9_SetResetPin(unsigned long ulVal);
	
	ECM_LIB_API extern SPI_CMD_PACKAGE_T g_SpiCmd;
	ECM_LIB_API extern SPI_RET_PACKAGE_T g_SpiRet;
	ECM_LIB_API extern SPI_CMD_PACKAGE_T g_SpiCmdBK;
	ECM_LIB_API extern SPI_RET_PACKAGE_T g_SpiRetBK;
	ECM_LIB_API extern uint32_t g_u32SpiPackSize;
	ECM_LIB_API extern uint32_t g_u32SpiDataSize;

	ECM_LIB_API int32_t ECM_RtaInit(uint8_t u8RuleCnt, uint8_t u8BufSize);
	ECM_LIB_API void ECM_RtaClose();
	ECM_LIB_API void ECM_RtaSetEnable(uint8_t u8Enable);
	ECM_LIB_API int32_t ECM_RtaGetRunFlag(uint8_t *pu8RtaRuning);
	ECM_LIB_API int32_t ECM_RtaSetBuf(uint8_t u8Offset, uint8_t u8Size, uint8_t *pBuf);
	ECM_LIB_API int32_t ECM_RtaGetBuf(uint8_t u8Offset, uint8_t u8Size, uint8_t *pBuf, uint8_t *pCondition);
	ECM_LIB_API int32_t ECM_RtaSetRule(uint8_t u8Idx, uint8_t u8Cnt, RTA_RULE *pRule);

	ECM_LIB_API int32_t ECM_AllocODOEList(void);
	ECM_LIB_API int32_t ECM_FreeODOEList(void);
	ECM_LIB_API int32_t ECM_ReadODList(uint8_t slave);
	ECM_LIB_API int32_t ECM_ReadODDesc(uint16_t item);
	ECM_LIB_API int32_t ECM_ReadOE(uint16_t item, uint8_t subindex);
	ECM_LIB_API int32_t ECM_GetODEntries(uint16_t *pu16);
	ECM_LIB_API int32_t ECM_GetODIndex(uint16_t *pu16, int16_t entry);
	ECM_LIB_API int32_t ECM_GetODDatatype(uint16_t *pu16);
	ECM_LIB_API int32_t ECM_GetODObjcode(uint8_t *pu8);
	ECM_LIB_API int32_t ECM_GetODMaxSub(uint8_t *pu8);
	ECM_LIB_API int32_t ECM_GetODName(char *str);
	ECM_LIB_API int32_t ECM_GetOEValueInfo(uint8_t *pu8);
	ECM_LIB_API int32_t ECM_GetOEDataType(uint16_t *pu16);
	ECM_LIB_API int32_t ECM_GetOEBitLength(uint16_t *pu16);
	ECM_LIB_API int32_t ECM_GetOEObjAccess(uint16_t *pu16);
	ECM_LIB_API int32_t ECM_GetOEName(char *str);

	ECM_LIB_API int32_t ECM_EcatFoEDownload(
		uint8_t slave,
		char *filename,
		uint32_t password,
		uint32_t filesize,
		uint32_t startaddr,
		uint32_t timeout );
	ECM_LIB_API int32_t ECM_EcatFoEUpload(
		uint8_t slave,
		char *filename,
		uint32_t password,
		uint32_t *pFilesize,
		uint32_t startaddr,
		uint32_t timeout );
	ECM_LIB_API int32_t ECM_EcatFoEDataWrite(uint32_t addr, uint32_t datasize, uint8_t *pBuf);
	ECM_LIB_API int32_t ECM_EcatFoEDataRead(uint32_t addr, uint32_t *pDatasize, uint8_t *pBuf);
	ECM_LIB_API int32_t ECM_EcatFoEDataGet(uint8_t *pBuf);
	/*
	 * For version 0x1F or later
	 */
	ECM_LIB_API int32_t ECM_MDIOWrite(uint8_t u8Reg, uint8_t u8Addr, uint16_t u16Data);
	ECM_LIB_API int32_t ECM_MDIORead(uint8_t u8Reg, uint8_t u8Addr, uint16_t *pu16Data);
	ECM_LIB_API int32_t ECM_DirectAssign(uint8_t u8Slv, uint8_t u8Mode, uint16_t u16Obits, uint16_t u16Ibits);
	ECM_LIB_API void ECM_SetEcatInitMode(uint8_t u8mode);
	/*
	* For version 0x21 or later
	*/
	ECM_LIB_API int32_t ECM_EnableLRW(uint8_t u8enable);
	ECM_LIB_API int32_t ECM_CoeEmergency(uint8_t num, coe_emergency_t *coe_emergency);
	ECM_LIB_API int32_t ECM_RecvMbxReq(uint8_t u8slave);
	ECM_LIB_API int32_t ECM_RecvMbxGet(uint16_t u16offset, uint16_t u16size, uint8_t* buf);
	ECM_LIB_API int32_t ECM_MbxStatGet(uint8_t* buf);
	ECM_LIB_API int32_t ECM_EnbleMbxStatPolling(uint16_t u16size, uint8_t* buf);


	ECM_LIB_API int32_t ECM_SetFifoThreshold(uint8_t u8enableTx, uint8_t u8enableRx, uint8_t u8TxThreshold, uint8_t u8RxThreshold);

#ifdef __cplusplus
}
#endif

#endif
