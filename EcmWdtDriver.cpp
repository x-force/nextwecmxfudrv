/******************************************************************************
*
* @copyright (C) 2020 NEXTW TECHNOLOGY CO., LTD.. All rights reserved.
*
* Sample Watchdog Driver for ECM-XF
*
******************************************************************************/
#include "stdafx.h"
#include "EcmUsrDriver.h"
#include "EcmWdtDriver.h"

extern CRITICAL_SECTION g_CriticalSection;
extern SPI_CMD_PACKAGE_T g_SpiCmd;
extern SPI_RET_PACKAGE_T g_SpiRet;

int32_t EcmCmdTransceiver(int32_t nTryCnt);

int ECM_WdtOpen(uint32_t timeout, uint32_t resetdelay)
{
	int32_t nret;
	EnterCriticalSection(&g_CriticalSection);
	g_SpiCmd.pHead->u8Cmd = ECM_WDT_FUNC_OP;
	g_SpiCmd.pHead->u8Param = 0;
	g_SpiCmd.pHead->u16Size = 8;
	memcpy(g_SpiCmd.Data, &timeout, sizeof(uint32_t));
	memcpy(&g_SpiCmd.Data[4], &resetdelay, sizeof(uint32_t));
	g_SpiCmd.pHead->u8Idx++;
	nret = EcmCmdTransceiver(1000);
	LeaveCriticalSection(&g_CriticalSection);
	return nret;
}

int ECM_WdtClose(void)
{
	int32_t nret;
	EnterCriticalSection(&g_CriticalSection);
	g_SpiCmd.pHead->u8Cmd = ECM_WDT_FUNC_OP;
	g_SpiCmd.pHead->u8Param = 1;
	g_SpiCmd.pHead->u16Size = 0;
	g_SpiCmd.pHead->u8Idx++;
	nret = EcmCmdTransceiver(1000);
	LeaveCriticalSection(&g_CriticalSection);
	return nret;
}

int ECM_WdtResetCounter(void)
{
	int32_t nret;
	EnterCriticalSection(&g_CriticalSection);
	g_SpiCmd.pHead->u8Cmd = ECM_WDT_FUNC_OP;
	g_SpiCmd.pHead->u8Param = 2;
	g_SpiCmd.pHead->u16Size = 0;
	g_SpiCmd.pHead->u8Idx++;
	nret = EcmCmdTransceiver(1000);
	LeaveCriticalSection(&g_CriticalSection);
	return nret;
}

int ECM_WdtClearResetFlag(void)
{
	int32_t nret;
	EnterCriticalSection(&g_CriticalSection);
	g_SpiCmd.pHead->u8Cmd = ECM_WDT_FUNC_OP;
	g_SpiCmd.pHead->u8Param = 3;
	g_SpiCmd.pHead->u16Size = 0;
	g_SpiCmd.pHead->u8Idx++;
	nret = EcmCmdTransceiver(1000);
	LeaveCriticalSection(&g_CriticalSection);
	return nret;
}

int ECM_WdtGetResetFlag(uint32_t *flag)
{
	int32_t nret;
	EnterCriticalSection(&g_CriticalSection);
	g_SpiCmd.pHead->u8Cmd = ECM_WDT_FUNC_OP;
	g_SpiCmd.pHead->u8Param = 4;
	g_SpiCmd.pHead->u16Size = 0;
	g_SpiCmd.pHead->u8Idx++;
	nret = EcmCmdTransceiver(1000);
	if (nret == 1) {
		memcpy(flag, g_SpiRet.Data, 4);
	}
	LeaveCriticalSection(&g_CriticalSection);
	return nret;
}

int ECM_WdtClearTimeoutFlag(void)
{
	int32_t nret;
	EnterCriticalSection(&g_CriticalSection);
	g_SpiCmd.pHead->u8Cmd = ECM_WDT_FUNC_OP;
	g_SpiCmd.pHead->u8Param = 5;
	g_SpiCmd.pHead->u16Size = 0;
	g_SpiCmd.pHead->u8Idx++;
	nret = EcmCmdTransceiver(1000);
	LeaveCriticalSection(&g_CriticalSection);
	return nret;
}

int ECM_WdtGetTimeoutFlag(uint32_t *flag)
{
	int32_t nret;
	EnterCriticalSection(&g_CriticalSection);
	g_SpiCmd.pHead->u8Cmd = ECM_WDT_FUNC_OP;
	g_SpiCmd.pHead->u8Param = 6;
	g_SpiCmd.pHead->u16Size = 0;
	g_SpiCmd.pHead->u8Idx++;
	nret = EcmCmdTransceiver(1000);
	if (nret == 1) {
		memcpy(flag, g_SpiRet.Data, 4);
	}
	LeaveCriticalSection(&g_CriticalSection);
	return nret;
}

int ECM_WdtEnableInt(void)
{
	int32_t nret;
	EnterCriticalSection(&g_CriticalSection);
	g_SpiCmd.pHead->u8Cmd = ECM_WDT_FUNC_OP;
	g_SpiCmd.pHead->u8Param = 9;
	g_SpiCmd.pHead->u16Size = 0;
	g_SpiCmd.pHead->u8Idx++;
	nret = EcmCmdTransceiver(1000);
	LeaveCriticalSection(&g_CriticalSection);
	return nret;
}

int ECM_WdtDisableInt(void)
{
	int32_t nret;
	EnterCriticalSection(&g_CriticalSection);
	g_SpiCmd.pHead->u8Cmd = ECM_WDT_FUNC_OP;
	g_SpiCmd.pHead->u8Param = 10;
	g_SpiCmd.pHead->u16Size = 0;
	g_SpiCmd.pHead->u8Idx++;
	nret = EcmCmdTransceiver(1000);
	LeaveCriticalSection(&g_CriticalSection);
	return nret;
}

int ECM_WdtCmdResetCounter(uint8_t u8enable)
{
	int32_t nret;
	EnterCriticalSection(&g_CriticalSection);
	g_SpiCmd.pHead->u8Cmd = ECM_WDT_FUNC_OP;
	g_SpiCmd.pHead->u8Param = 11;
	g_SpiCmd.pHead->u16Size = 0;
	g_SpiCmd.Data[0] = u8enable;
	g_SpiCmd.Data[1] = 0;
	g_SpiCmd.Data[2] = 0;
	g_SpiCmd.Data[3] = 0;
	g_SpiCmd.pHead->u8Idx++;
	nret = EcmCmdTransceiver(1000);
	LeaveCriticalSection(&g_CriticalSection);
	return nret;
}

int ECM_WdtOpenEx(uint32_t timeout, uint32_t resetdelay, uint8_t u8EnableCmdReset)
{
	int32_t nret;
	EnterCriticalSection(&g_CriticalSection);
	g_SpiCmd.pHead->u8Cmd = ECM_WDT_FUNC_OP;
	g_SpiCmd.pHead->u8Param = 12;
	g_SpiCmd.pHead->u16Size = 12;
	memcpy(g_SpiCmd.Data, &timeout, sizeof(uint32_t));
	memcpy(&g_SpiCmd.Data[4], &resetdelay, sizeof(uint32_t));
	memcpy(&g_SpiCmd.Data[8], &u8EnableCmdReset, sizeof(uint8_t));
	g_SpiCmd.pHead->u8Idx++;
	nret = EcmCmdTransceiver(1000);
	LeaveCriticalSection(&g_CriticalSection);
	return nret;
}