/******************************************************************************
 *	File	:	EcmDacDriver.cpp
 *	Version :	1.7
 *	Date	:	2022/07/27
 *	Author	:	XFORCE
 *
 *	ECM-XF DAC driver example
 *
 *	Demonstrate how to implement API type user driver
 *
 * @copyright (C) 2020 NEXTW TECHNOLOGY CO., LTD.. All rights reserved.
 *
 ******************************************************************************/#include "EcmUsrDriver.h"
#include "stdafx.h"
#include <stdio.h>
#include "EcmUsrDriver.h"
#include "EcmDacDriver.h"
#include "platform.h"

extern CRITICAL_SECTION g_CriticalSection;
//extern SPI_CMD_PACKAGE_T *pCmd;
//extern SPI_RET_PACKAGE_T *pRet;

int ECM_DacOpen( uint32_t mode)
{
	int i=0;
	uint8_t IdxCheck;
	EnterCriticalSection(&g_CriticalSection);
	g_SpiCmd.pHead->u8Cmd = ECM_DAC_FUNC_OP;
	g_SpiCmd.pHead->u8Param = 20;
	g_SpiCmd.pHead->u16Size = 4;
	memcpy( g_SpiCmd.Data, &mode, sizeof(uint32_t));
	g_SpiCmd.pHead->u8Idx++;
	for(i=0;i<100;i++){
		if(SpiDataExchange(&IdxCheck, 0)){
			if(g_SpiCmd.pHead->u8Idx == IdxCheck){
				LeaveCriticalSection(&g_CriticalSection);
				return 1;
			}
		}
	}
	LeaveCriticalSection(&g_CriticalSection);
	return 0;
}

int ECM_DacClose(void)
{
	int i=0;
	uint8_t IdxCheck;
	EnterCriticalSection(&g_CriticalSection);
	g_SpiCmd.pHead->u8Cmd = ECM_DAC_FUNC_OP;
	g_SpiCmd.pHead->u8Param = 21;
	g_SpiCmd.pHead->u16Size = 0;
	g_SpiCmd.pHead->u8Idx++;
	for(i=0;i<100;i++){
		if(SpiDataExchange(&IdxCheck, 0)){
			if(g_SpiCmd.pHead->u8Idx == IdxCheck){
				LeaveCriticalSection(&g_CriticalSection);
				return 1;
			}
		}
	}
	LeaveCriticalSection(&g_CriticalSection);
	return 0;
}

int ECM_DacSetDelayTime( uint32_t delay)
{
	int i=0;
	uint8_t IdxCheck;
	EnterCriticalSection(&g_CriticalSection);
	g_SpiCmd.pHead->u8Cmd = ECM_DAC_FUNC_OP;
	g_SpiCmd.pHead->u8Param = 22;
	g_SpiCmd.pHead->u16Size = 4;
	memcpy( g_SpiCmd.Data, &delay, sizeof(uint32_t));
	g_SpiCmd.pHead->u8Idx++;
	for(i=0;i<100;i++){
		if(SpiDataExchange(&IdxCheck, 0)){
			if(g_SpiCmd.pHead->u8Idx == IdxCheck){
				LeaveCriticalSection(&g_CriticalSection);
				return 1;
			}
		}
	}
	LeaveCriticalSection(&g_CriticalSection);
	return 0;
}

int ECM_DacSetData( uint32_t data)
{
	int i=0;
	uint8_t IdxCheck;
	EnterCriticalSection(&g_CriticalSection);
	g_SpiCmd.pHead->u8Cmd = ECM_DAC_FUNC_OP;
	g_SpiCmd.pHead->u8Param = 11;
	g_SpiCmd.pHead->u16Size = 4;
	memcpy( g_SpiCmd.Data, &data, sizeof(uint32_t));
	g_SpiCmd.pHead->u8Idx++;
	for(i=0;i<100;i++){
		if(SpiDataExchange(&IdxCheck, 0)){
			if(g_SpiCmd.pHead->u8Idx == IdxCheck){
				LeaveCriticalSection(&g_CriticalSection);
				return 1;
			}
		}
	}
	LeaveCriticalSection(&g_CriticalSection);
	return 0;
}

int ECM_DacStartConv(void)
{
	int i=0;
	uint8_t IdxCheck;
	EnterCriticalSection(&g_CriticalSection);
	g_SpiCmd.pHead->u8Cmd = ECM_DAC_FUNC_OP;
	g_SpiCmd.pHead->u8Param = 0;
	g_SpiCmd.pHead->u16Size = 0;
	g_SpiCmd.pHead->u8Idx++;
	for(i=0;i<100;i++){
		if(SpiDataExchange(&IdxCheck, 0)){
			if(g_SpiCmd.pHead->u8Idx == IdxCheck){
				LeaveCriticalSection(&g_CriticalSection);
				return 1;
			}
		}
	}
	LeaveCriticalSection(&g_CriticalSection);
	return 0;
}
