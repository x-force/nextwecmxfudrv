/******************************************************************************
 *	File	:	EcmAdcDriver.h
 *	Version :	1.7
 *	Date	:	2022/07/27
 *	Author	:	XFORCE
 *
 *	ECM-XF ADC driver example - Header file
 *
 *	Demonstrate how to implement API type user driver
 *
 * @copyright (C) 2020 NEXTW TECHNOLOGY CO., LTD.. All rights reserved.
 *
 ******************************************************************************/
#ifndef _ECMADCDRIVER_H_
#define _ECMADCDRIVER_H_
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "EcmDriver.h"
#ifdef __cplusplus
extern "C" {
#endif
	/******************************************************************************
	 * ADC_MODE Definitions
	 ******************************************************************************/
#define ECM_ADC_MODE_SINGLE_END         (0UL)   /* Single-end input mode  */
#define ECM_ADC_MODE_DIFFERENTIAL       (1UL)   /* Differential input mode */

	 /******************************************************************************
	  * ADC_TRIG Definitions
	  ******************************************************************************/
#define ECM_ADC_TRIG_SOFTWARE           (0UL)      /* Software trigger. Use ECM_AdcStartConv() function to trigger ADC input */

	  /**
	   * Open ADC device and Set ADC input mode
	   *
	   * @param  mode				: ADC mode. See ECM_ADC_MODE_## definitions.
	   *
	   * @return					: 1 for success; 0 for timeout; -1 for invalid input parameters.
	   */
	ECM_LIB_API int ECM_AdcOpen(uint32_t mode);

	/**
	 * Open ADC device and Set ADC input mode
	 *
	 * @param  mode				: ADC mode. See ECM_ADC_MODE_## definitions.
	 *
	 * @return					: 1 for success; 0 for timeout.
	 */
	ECM_LIB_API int ECM_AdcClose(void);

	/**
	 * Select ADC trigger source
	 *
	 * @param  source			: ADC trigger source. See ECM_ADC_TRIG_## definitions.
	 *
	 * @return					: 1 for success; 0 for timeout; -1 for invalid input parameters.
	 */
	ECM_LIB_API int ECM_AdcConfigSampleModule(uint32_t source);

	/**
	 * Get ADC data valid flag
	 *
	 * @param  flag 			: ADC data valid flag. The data is valid if the flag is non-zero.
	 *
	 * @return					: 1 for success; 0 for timeout; -1 for invalid input parameters.
	 */
	ECM_LIB_API int ECM_AdcGetDataValidFlag(uint32_t *flag);

	/**
	 * Start ADC convert.
	 *
	 * @return					: 1 for success; 0 for timeout; -1 for invalid input parameters.
	 */
	ECM_LIB_API int ECM_AdcStartConv(void);

	/**
	 * Get ADC data value
	 *
	 * @param  data 			: ADC data value.
	 *
	 * @return					: 1 for success; 0 for timeout; -1 for invalid input parameters.
	 */
	ECM_LIB_API int ECM_AdcGetConvData(uint32_t *data);

#ifdef __cplusplus
}
#endif

#endif /* _ECMADCDRIVER_H_ */
