﻿/******************************************************************************
 *	File	:	SimpleTest.cpp
 *	Version :	1.7
 *	Date	:	2022/07/27
 *	Author	:	User
 *
 *	Hardware dependency function - C source file
 *
 *	Provide necessary function for EcmUsrDriver
 *
 * @copyright (C) 2020 NEXTW TECHNOLOGY CO., LTD.. All rights reserved.
 *
 ******************************************************************************/
#include <windows.h>
#include <iostream>
#include <platform.h>
#include <EcmUsrDriver.h>
uint8_t g_u8RxPDO[1400];
uint8_t g_u8TxPDO[1400];

int main()
{
	int bLinkPin;
	unsigned short u16SpiSize = 992;
	uint8_t u8State = 0, u8Version = 0, u8TmpEcState = 0, u8CrcErrCnt = 0, u8LastCrcErrCnt = 0, u8FifoCnt = 0;
	uint16_t u16Data, u16InfoBuf, u16RxPDOSize, u16TxPDOSize;
	int i = 0, nRet = 0;
	int nSlvCntOnline;
	memset(g_u8RxPDO, 0, sizeof(g_u8RxPDO));
	memset(g_u8TxPDO, 0, sizeof(g_u8TxPDO));
	// 第一個參數為 0 for USB
	// 第一個參數為 1 for PCIe
	XF_SetDevType(1);
	// 初始化PCIe晶片
	//
	nRet = UserSys_Init(24000000);
	if (nRet < 0) {
		printf("UserSys_Init return %d\n", nRet);
		return -1;
	}
	u8Version = ECM_InitLibrary(&u16SpiSize, ECM_CRC_TYPE_NONE);
	printf("u8Version 0x%X\n", u8Version);
	if (u8Version == 0)
		return 0;
	nRet = ECM_GetNetLinkPin(&bLinkPin);
	printf("ECM_GetNetLinkPin linkpin(%d) ret(%d)\n", bLinkPin, nRet);

	nRet = ECM_EcatInit(0x300, 1000000);
	printf("ECM_EcatInit %d\n", nRet);
	
	nSlvCntOnline = ECM_EcatSlvCntGet();
	printf("Slave %d\n", nSlvCntOnline);
	for (i = 0; i < nSlvCntOnline; i++) {
		nRet = ECM_SlaveInfoGet(i, ECM_SLV_INFO_ALstatuscode, (uint8_t *)&u16InfoBuf);
		if (nRet > 0) {
			if (u16InfoBuf != 0) {
				printf("Slave %d AL StatusCode(%d)...\n", i, u16InfoBuf);
			}
		}
		else {
			printf("Slave %d Get AL StatusCode failed...\n", i);
		}
	}
	ECM_SetTxFIFOCnt(64);
	ECM_SetRxFIFOCnt(64);
	ECM_InitFIFO();

	u16RxPDOSize = ECM_FifoRxPdoSizeGet();
	printf("RxPDOSize %dByte(s)\n", u16RxPDOSize);
	u16TxPDOSize = ECM_FifoTxPdoSizeGet();
	printf("TxPDOSize %dByte(s)\n", u16TxPDOSize);

	nRet = ECM_EcatStateUpdate();
	if (nRet <= 0) {
		printf("ECM_EcatStateUpdate fail(%d)\n", nRet);
	}
	else {
		printf("ECM_EcatStateUpdate done\n");
	}
	for (i = 0; i < nSlvCntOnline; i++) {
		nRet = ECM_EcatStateGet(i, &u8TmpEcState);
		if (nRet) {
			printf("(%d) EcState(0x%X)\n", i, u8TmpEcState);
		}
	}
	printf("Wait key\n");
	getchar();
	ECM_EcatStateSet(0xFF, EC_STATE_SAFE_OP);
	Sleep(1000);
	nRet = ECM_EcatStateGet(0xFF, &u8TmpEcState);
	if (nRet) {
		printf("EcState(0x%X)\n", u8TmpEcState);
	}
	ECM_EcatStateSet(0xFF, EC_STATE_OPERATIONAL);
	for (i = 0; i < 10; i++) {
		Sleep(1000);
		nRet = ECM_EcatStateGet(0xFF, &u8TmpEcState);
		if (nRet) {
			printf("EcState(0x%X)\n", u8TmpEcState);
		}
		if (u8TmpEcState == 8) {
			break;
		}
	}
	if (u8TmpEcState == 8) {
		u8CrcErrCnt = 0;
		for (i = 0; i < 100000; /*i++*/) {
			if (u8FifoCnt < 64 - 3) {
				nRet = ECM_EcatPdoFifoDataExchangeAdv(ECM_FIFO_RW, 1, g_u8RxPDO, g_u8TxPDO, u16RxPDOSize, &u8FifoCnt, &u8CrcErrCnt, NULL, NULL);
				if (nRet <= 4) {
					printf("(%d)\tFifoCnt %d Crc %d Ret %d\r\n", i, u8FifoCnt, u8CrcErrCnt, nRet);
				}
				else {
					i++;
				}
			}
			else {
				nRet = ECM_EcatPdoFifoDataExchangeAdv(ECM_FIFO_RD, 1, g_u8RxPDO, g_u8TxPDO, u16RxPDOSize, &u8FifoCnt, &u8CrcErrCnt, NULL, NULL);
				printf("(%d)\tFifoCnt %d Crc %d Ret %d\r", i, u8FifoCnt, u8CrcErrCnt, nRet);
				Sleep(10);
			}
		}
		printf("Finish CrcErrCnt %d\n", u8CrcErrCnt);
	}
	ECM_EcatStateSet(0xFF, EC_STATE_SAFE_OP);
	Sleep(1000);
	ECM_EcatStateSet(0xFF, EC_STATE_PRE_OP);
	Sleep(1000);
	ECM_CloseLibrary();
	UserSys_Close();
}

// 執行程式: Ctrl + F5 或 [偵錯] > [啟動但不偵錯] 功能表
// 偵錯程式: F5 或 [偵錯] > [啟動偵錯] 功能表

// 開始使用的提示: 
//   1. 使用 [方案總管] 視窗，新增/管理檔案
//   2. 使用 [Team Explorer] 視窗，連線到原始檔控制
//   3. 使用 [輸出] 視窗，參閱組建輸出與其他訊息
//   4. 使用 [錯誤清單] 視窗，檢視錯誤
//   5. 前往 [專案] > [新增項目]，建立新的程式碼檔案，或是前往 [專案] > [新增現有項目]，將現有程式碼檔案新增至專案
//   6. 之後要再次開啟此專案時，請前往 [檔案] > [開啟] > [專案]，然後選取 .sln 檔案
