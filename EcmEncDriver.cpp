/******************************************************************************
 *	File	:	EcmEncDriver.cpp
 *	Version :	1.7
 *	Date	:	2022/07/27
 *	Author	:	XFORCE
 *
 *	ECM-XF ENC driver example
 *
 *	Demonstrate how to implement API type user driver
 *
 * @copyright (C) 2020 NEXTW TECHNOLOGY CO., LTD.. All rights reserved.
 *
 ******************************************************************************/#include "EcmUsrDriver.h"
#include "stdafx.h"
#include "EcmUsrDriver.h"
#include "EcmEncDriver.h"

extern CRITICAL_SECTION g_CriticalSection;
//extern SPI_CMD_PACKAGE_T *pCmd;
//extern SPI_RET_PACKAGE_T *pRet;

int ECM_EncOpen( uint8_t mode)
{
	int i=0;
	uint8_t IdxCheck;
    if(mode > 3)
        return -1;
	EnterCriticalSection(&g_CriticalSection);
	g_SpiCmd.pHead->u8Cmd = ECM_QEI_FUNC_OP;
	g_SpiCmd.pHead->u8Param = 4;
	g_SpiCmd.pHead->u16Size = 4;
	g_SpiCmd.Data[0] = g_SpiCmd.Data[2] = g_SpiCmd.Data[3] = 0;
	g_SpiCmd.Data[1] = mode;
	g_SpiCmd.pHead->u8Idx++;
	for(i=0;i<100;i++){
		if(SpiDataExchange(&IdxCheck, 0)){
			if(g_SpiCmd.pHead->u8Idx == IdxCheck){
				LeaveCriticalSection(&g_CriticalSection);
				return 1;
			}
		}
	}
	LeaveCriticalSection(&g_CriticalSection);
	return 0;
}

int ECM_EncClose()
{
	int i = 0;
	uint8_t IdxCheck;
	EnterCriticalSection(&g_CriticalSection);
	g_SpiCmd.pHead->u8Cmd = ECM_QEI_FUNC_OP;
	g_SpiCmd.pHead->u8Param = 0;
	g_SpiCmd.pHead->u16Size = 4;
	g_SpiCmd.Data[0] = g_SpiCmd.Data[1] = g_SpiCmd.Data[2] = g_SpiCmd.Data[3] = 0;
	g_SpiCmd.pHead->u8Idx++;
	for (i = 0; i < 100; i++) {
		if (SpiDataExchange(&IdxCheck, 0)) {
			if (g_SpiCmd.pHead->u8Idx == IdxCheck) {
				LeaveCriticalSection(&g_CriticalSection);
				return 1;
			}
		}
	}
	LeaveCriticalSection(&g_CriticalSection);
	return 0;
}

int ECM_EncStart(void)
{
	int i=0;
	uint8_t IdxCheck;
	EnterCriticalSection(&g_CriticalSection);
	g_SpiCmd.pHead->u8Cmd = ECM_QEI_FUNC_OP;
	g_SpiCmd.pHead->u8Param = 5;
	g_SpiCmd.pHead->u16Size = 0;
	g_SpiCmd.pHead->u8Idx++;
	for(i=0;i<100;i++){
		if(SpiDataExchange(&IdxCheck, 0)){
			if(g_SpiCmd.pHead->u8Idx == IdxCheck){
				LeaveCriticalSection(&g_CriticalSection);
				return 1;
			}
		}
	}
	LeaveCriticalSection(&g_CriticalSection);
	return 0;
}

int ECM_EncStop(void)
{
	int i=0;
	uint8_t IdxCheck;
	EnterCriticalSection(&g_CriticalSection);
	g_SpiCmd.pHead->u8Cmd = ECM_QEI_FUNC_OP;
	g_SpiCmd.pHead->u8Param = 6;
	g_SpiCmd.pHead->u16Size = 0;
	g_SpiCmd.pHead->u8Idx++;
	for(i=0;i<100;i++){
		if(SpiDataExchange(&IdxCheck, 0)){
			if(g_SpiCmd.pHead->u8Idx == IdxCheck){
				LeaveCriticalSection(&g_CriticalSection);
				return 1;
			}
		}
	}
	LeaveCriticalSection(&g_CriticalSection);
	return 0;
}

int ECM_EncGetCount( uint32_t *count)
{
	int i=0;
	uint8_t IdxCheck;
	if(!count)
		return -1;
	EnterCriticalSection(&g_CriticalSection);
	g_SpiCmd.pHead->u8Cmd = ECM_QEI_FUNC_OP;
	g_SpiCmd.pHead->u8Param = 21;
	g_SpiCmd.pHead->u16Size = 0;
	g_SpiCmd.pHead->u8Idx++;
	for(i=0;i<100;i++){
		if(SpiDataExchange(&IdxCheck, 0)){
			if(g_SpiCmd.pHead->u8Idx == IdxCheck){
				memcpy( count, g_SpiRet.Data, sizeof(uint32_t));
				LeaveCriticalSection(&g_CriticalSection);
				return 1;
			}
		}
	}
	LeaveCriticalSection(&g_CriticalSection);
	return 0;
}
