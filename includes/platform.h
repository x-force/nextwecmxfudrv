/******************************************************************************
 *	File	:	platform.h
 *	Version :	0.2
 *	Date	:	2020/09/03
 *	Author	:	User
 *
 *	Hardware dependency function - Header file
 *
 *	Provide necessary function for EcmUsrDriver
 *
 * @copyright (C) 2020 NEXTW TECHNOLOGY CO., LTD.. All rights reserved.
 *
 ******************************************************************************/
#ifndef _ECM_PLATFORM_H_
#define _ECM_PLATFORM_H_
#include <stdio.h>
#include <stdint.h>

#define	ECM_CRC_MAGIC_NUM			0x12345678

#ifdef ECMPLATFORM_EXPORTS
#define ECMPLATFORM_API __declspec(dllexport)
#else
#define ECMPLATFORM_API __declspec(dllimport)
#endif
#ifdef __cplusplus
extern "C" {
#endif
#define ULONG unsigned long
//extern ECMPLATFORM_API int g_nDevType;
ECMPLATFORM_API int EcmLocker(int32_t n32Data, int32_t n32Val);
ECMPLATFORM_API int XF_SetDevType(int nType);
ECMPLATFORM_API int XF_GetDevType();
ECMPLATFORM_API int UserSys_Init(uint32_t u32SpiFreq);
ECMPLATFORM_API void UserSys_Close(void);
ECMPLATFORM_API int UserSpiDataExchange(uint8_t *pTxBuf, uint8_t *pRxBuf, uint32_t u32PackSize);
ECMPLATFORM_API int UserDelay(uint32_t MircoSec);
//
ECMPLATFORM_API int32_t UserSetGpioData_u32(unsigned long ulVal);
ECMPLATFORM_API int32_t UserGetGpioData_u32(unsigned long *pulVal);
ECMPLATFORM_API int32_t UserGetGpioDir(unsigned long *pulDir);
ECMPLATFORM_API int32_t UserSetGpioDir(unsigned long ulDir);
ECMPLATFORM_API int32_t UserSetGpioData(uint8_t u8Idx, unsigned long ulVal);
ECMPLATFORM_API int32_t UserGetGpioData(uint8_t u8Idx, unsigned long *pulVal);
/*ECMPLATFORM_API int32_t UserGetGpioDir(ULONG *pulDir);
ECMPLATFORM_API int32_t UserSetGpioDir(ULONG ulDir);
ECMPLATFORM_API int32_t UserSetGpioData(uint8_t u8Idx, ULONG ulVal);
ECMPLATFORM_API int32_t UserGetGpioData(uint8_t u8Idx, ULONG *pulVal);*/
#ifdef __cplusplus
}
#endif

#endif
//	(C) COPYRIGHT 2020 NEXTW Technology Corp.
