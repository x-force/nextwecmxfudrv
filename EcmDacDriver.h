/******************************************************************************
 *	File	:	EcmDacDriver.h
 *	Version :	1.7
 *	Date	:	2022/07/27
 *	Author	:	XFORCE
 *
 *	ECM-XF DAC driver example - Header file
 *
 *	Demonstrate how to implement API type user driver
 *
 * @copyright (C) 2020 NEXTW TECHNOLOGY CO., LTD.. All rights reserved.
 *
 ******************************************************************************/

#ifndef _ECMDACDRIVER_H_
#define _ECMDACDRIVER_H_
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "EcmDriver.h"

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************************************
 * DAC_MODE Definitions
 ******************************************************************************/
#define ECM_DAC_TRIG_MODE_WRTTE_DATA            0x0000UL        /* Write data trigger. Use ECM_DacSetData() function to trigger DAC output. */
#define ECM_DAC_TRIG_MODE_SOFTWARE              0x0010UL        /* software trigger. Use ECM_DacStartConv() function to trigger DAC output. */
#define ECM_DAC_TRIG_MODE_LOW_LEVEL             0x0030UL        /* Low-level trigger. DAC is triggered if DAC_ST pin is low. */
#define ECM_DAC_TRIG_MODE_HIGH_LEVEL            0x1030UL        /* High-level trigger. DAC is triggered if DAC_ST pin is high. */
#define ECM_DAC_TRIG_MODE_RISING_EDGE           0x2030UL        /* Rising-edge trigger. DAC is triggered if DAC_ST pin detect a rising edge. */
#define ECM_DAC_TRIG_MODE_FALLING_EDGE          0x3030UL        /* Falling-edge trigger. DAC is triggered if DAC_ST pin detect a falling edge. */


/**
 * Open DAC device and Set DAC trigger mode
 *
 * @param  mode				: ADC mode. See ECM_DAC_TRIG_MODE_## definitions.
 *
 * @return					: 1 for success; 0 for timeout; -1 for invalid input parameters.
 */
ECM_LIB_API int ECM_DacOpen( uint32_t mode);

/**
 * Close DAC device.
 *
 * @return					: 1 for success; 0 for timeout.
 */
ECM_LIB_API int ECM_DacClose(void);

/**
  * Set delay time for DAC to become stable.
 *
 * @param  delay			: Delay time in micro second. The range is 0 to 1023/96.
 *
 * @return					: 1 for success; 0 for timeout; -1 for invalid input parameters.
 */
ECM_LIB_API int ECM_DacSetDelayTime( uint32_t delay);

/**
 * Set DAC data value
 *
 * @param  data 			: DAC data value.
 *
 * @return					: 1 for success; 0 for timeout; -1 for invalid input parameters.
 */
ECM_LIB_API int ECM_DacSetData( uint32_t data);

/**
 * Start DAC convert.
 *
 * @return					: 1 for success; 0 for timeout.
 */
ECM_LIB_API int ECM_DacStartConv(void);

#ifdef __cplusplus
}
#endif

#endif /* _ECMDACDRIVER_H_ */
