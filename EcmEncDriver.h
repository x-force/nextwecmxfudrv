/******************************************************************************
 *	File	:	EcmEncDriver.h
 *	Version :	1.7
 *	Date	:	2022/07/27
 *	Author	:	XFORCE
 *
 *	ECM-XF ENC driver example - Header file
 *
 *	Demonstrate how to implement API type user driver
 *
 * @copyright (C) 2020 NEXTW TECHNOLOGY CO., LTD.. All rights reserved.
 *
 ******************************************************************************/

#ifndef _ECMENCDRIVER_H_
#define _ECMENCDRIVER_H_
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "EcmDriver.h"
#ifdef __cplusplus
extern "C" {
#endif
	/******************************************************************************
	 * ENC_MODE Definitions
	 ******************************************************************************/
#define ECM_ENC_MODE_X4_FREE            0x0UL /* X4 free-counting Mode */
#define ECM_ENC_MODE_X2_FREE            0x1UL /* X2 free-counting Mode */
#define ECM_ENC_MODE_X4_COMP            0x2UL /* X4 compare-counting Mode */
#define ECM_ENC_MODE_X2_COMP            0x3UL /* X2 compare-counting Mode */

	 /**
	  * Open and Set Encoder mode
	  *
	  * @param  mode				: Encoder mode. See ECM_ENC_MODE_## definitions.
	  *
	  * @return					: 1 for success; 0 for timeout; -1 for invalid input parameters.
	  */
	ECM_LIB_API int ECM_EncOpen(uint8_t mode);
	ECM_LIB_API int ECM_EncClose(void);
	/**
	 * Start Encoder counting
	 *
	 * @return					: 1 for success; 0 for timeout.
	 */
	ECM_LIB_API int ECM_EncStart(void);

	/**
	 * Stop Encoder counting
	 *
	 * @return					: 1 for success; 0 for timeout.
	 */
	ECM_LIB_API int ECM_EncStop(void);

	/**
	 * Get Encoder counter value
	 *
	 * @param  count			: Encoder counter value pointer. The encoder counter value is filled in *(@count) if success.
	 *
	 * @return					: 1 for success; 0 for timeout; -1 for invalid input parameters.
	 */
	ECM_LIB_API int ECM_EncGetCount(uint32_t *count);

#ifdef __cplusplus
}
#endif

#endif
