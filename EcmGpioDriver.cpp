/******************************************************************************
 *	File	:	EcmGpioDriver.cpp
 *	Version :	1.7
 *	Date	:	2022/07/27
 *	Author	:	XFORCE
 *
 *	ECM-XF GPIO driver example
 *
 *	Demonstrate how to implement API type user driver
 *
 * @copyright (C) 2020 NEXTW TECHNOLOGY CO., LTD.. All rights reserved.
 *
 ******************************************************************************/
#include "stdafx.h"
#include "EcmUsrDriver.h"
#include "EcmGpioDriver.h"

extern CRITICAL_SECTION g_CriticalSection;
//extern SPI_CMD_PACKAGE_T *pCmd;
//extern SPI_RET_PACKAGE_T *pRet;
static uint8_t g_u8GpioDir = 0;
static uint8_t g_u8GpioMode = 0;
int ECM_GpioSetMode( uint8_t ch, uint8_t direction, uint8_t pusel)
{
	int i=0;
	uint8_t IdxCheck;
	EnterCriticalSection(&g_CriticalSection);
	g_SpiCmd.pHead->u8Cmd = ECM_GPIO_CONFIG_SET;
	g_SpiCmd.pHead->u8Param = 0;
	g_SpiCmd.pHead->u16Size = 0;
	if(ch < 8){
		g_SpiCmd.pHead->u8Data[0] = 1u << ch;
		g_SpiCmd.pHead->u8Data[1] = 0;
	}else if (ch < 16){
		g_SpiCmd.pHead->u8Data[1] = 1u << (ch-8);
		g_SpiCmd.pHead->u8Data[0] = 0;
	}else if( ch < 20){
        g_SpiCmd.pHead->u8Param = 8;
		g_SpiCmd.pHead->u8Data[0] = 1u << (ch-16);
		g_SpiCmd.pHead->u8Data[1] = 0;        
	}
	else {
		LeaveCriticalSection(&g_CriticalSection);
		return -1;
	}
	g_SpiCmd.pHead->u8Data[2] = direction & 3u;
	g_SpiCmd.pHead->u8Data[3] = pusel & 3u;
	g_SpiCmd.pHead->u8Idx++;
	for(i=0;i<100;i++){
		if(SpiDataExchange(&IdxCheck, 0)){
			if(g_SpiCmd.pHead->u8Idx == IdxCheck){
				LeaveCriticalSection(&g_CriticalSection);
				return 1;
			}
		}
	}
	LeaveCriticalSection(&g_CriticalSection);
	return 0;
}

int ECM_GpioEnableDebounce( uint8_t ch, uint8_t enable)
{
	int i=0;
	uint8_t IdxCheck;
	EnterCriticalSection(&g_CriticalSection);
	g_SpiCmd.pHead->u8Cmd = ECM_GPIO_CONFIG_SET;
	g_SpiCmd.pHead->u8Param = 2;
	g_SpiCmd.pHead->u16Size = 0;
	if(ch < 8){
		g_SpiCmd.pHead->u8Data[0] = 1u << ch;
		g_SpiCmd.pHead->u8Data[1] = 0;
	}else if (ch < 16){
		g_SpiCmd.pHead->u8Data[1] = 1u << (ch-8);
		g_SpiCmd.pHead->u8Data[0] = 0;
	}else if( ch < 20){
        g_SpiCmd.pHead->u8Param = 9;
		g_SpiCmd.pHead->u8Data[0] = 1u << (ch-16);
		g_SpiCmd.pHead->u8Data[1] = 0;        
	}
	else {
		LeaveCriticalSection(&g_CriticalSection);
		return -1;
	}
	g_SpiCmd.pHead->u8Data[2] = enable & 1u;
	g_SpiCmd.pHead->u8Idx++;
	for(i=0;i<100;i++){
		if(SpiDataExchange(&IdxCheck, 0)){
			if(g_SpiCmd.pHead->u8Idx == IdxCheck){
				LeaveCriticalSection(&g_CriticalSection);
				return 1;
			}
		}
	}
	LeaveCriticalSection(&g_CriticalSection);
	return 0;
}

int ECM_GpioSetDebounceClock( uint8_t source, uint8_t clock)
{
	int i=0;
	uint8_t IdxCheck;
	EnterCriticalSection(&g_CriticalSection);
	g_SpiCmd.pHead->u8Cmd = ECM_GPIO_CONFIG_SET;
	g_SpiCmd.pHead->u8Param = 3;
	g_SpiCmd.pHead->u16Size = 0;
	g_SpiCmd.pHead->u8Data[0] = (source & 1u) << 4 | (clock & 0xfu);
	g_SpiCmd.pHead->u8Idx++;
	for(i=0;i<100;i++){
		if(SpiDataExchange(&IdxCheck, 0)){
			if(g_SpiCmd.pHead->u8Idx == IdxCheck){
				LeaveCriticalSection(&g_CriticalSection);
				return 1;
			}
		}
	}
	LeaveCriticalSection(&g_CriticalSection);
	return 0;
}

int ECM_GpioSetValue( uint16_t val)
{
	int i=0;
	uint8_t IdxCheck;
	EnterCriticalSection(&g_CriticalSection);
	g_SpiCmd.pHead->u8Cmd = ECM_GPIO_FUNC_OP;
	g_SpiCmd.pHead->u8Param = 0;
	g_SpiCmd.pHead->u16Size = 0;
	g_SpiCmd.pHead->u8Data[0] = val & 0xff;
	g_SpiCmd.pHead->u8Data[1] = (val>>8) & 0xff;
	g_SpiCmd.pHead->u8Idx++;
	for(i=0;i<100;i++){
		if(SpiDataExchange(&IdxCheck, 0)){
			if(g_SpiCmd.pHead->u8Idx == IdxCheck){
				LeaveCriticalSection(&g_CriticalSection);
				return 1;
			}
		}
	}
	LeaveCriticalSection(&g_CriticalSection);
	return 0;
}

int ECM_GpioExtSetValue( uint32_t val)
{
	int i=0;
	uint8_t IdxCheck;
	EnterCriticalSection(&g_CriticalSection);
	g_SpiCmd.pHead->u8Cmd = ECM_GPIO_FUNC_OP;
	g_SpiCmd.pHead->u8Param = 2;
	g_SpiCmd.pHead->u16Size = 0;
	g_SpiCmd.pHead->u8Data[0] = val & 0xff;
	g_SpiCmd.pHead->u8Data[1] = (val>>8) & 0xff;
    g_SpiCmd.pHead->u8Data[2] = (val>>16) & 0xff;
	g_SpiCmd.pHead->u8Idx++;
	for(i=0;i<100;i++){
		if(SpiDataExchange(&IdxCheck, 0)){
			if(g_SpiCmd.pHead->u8Idx == IdxCheck){
				LeaveCriticalSection(&g_CriticalSection);
				return 1;
			}
		}
	}
	LeaveCriticalSection(&g_CriticalSection);
	return 0;
}

int ECM_GpioGetValue( uint16_t *val)
{
	int i=0;
	uint8_t IdxCheck;
	if(!val)
		return -1;
	EnterCriticalSection(&g_CriticalSection);
	g_SpiCmd.pHead->u8Cmd = ECM_GPIO_FUNC_OP;
	g_SpiCmd.pHead->u8Param = 1;
	g_SpiCmd.pHead->u16Size = 0;
	g_SpiCmd.pHead->u8Idx++;
	for(i=0;i<100;i++){
		if(SpiDataExchange(&IdxCheck, 0)){
			if(g_SpiCmd.pHead->u8Idx == IdxCheck){
				*val = g_SpiRet.Data[0] | (g_SpiRet.Data[1] << 8);
				LeaveCriticalSection(&g_CriticalSection);
				return 1;
			}
		}
	}
	LeaveCriticalSection(&g_CriticalSection);
	return 0;
}

int ECM_GpioExtGetValue( uint32_t *val)
{
	int i=0;
	uint8_t IdxCheck;
	if(!val)
		return -1;
	EnterCriticalSection(&g_CriticalSection);
	g_SpiCmd.pHead->u8Cmd = ECM_GPIO_FUNC_OP;
	g_SpiCmd.pHead->u8Param = 3;
	g_SpiCmd.pHead->u16Size = 0;
	g_SpiCmd.pHead->u8Idx++;
	for(i=0;i<100;i++){
		if(SpiDataExchange(&IdxCheck, 0)){
			if(g_SpiCmd.pHead->u8Idx == IdxCheck){
				*val = g_SpiRet.Data[0] | (g_SpiRet.Data[1] << 8) | (g_SpiRet.Data[2] << 16);
				LeaveCriticalSection(&g_CriticalSection);
				return 1;
			}
		}
	}
	LeaveCriticalSection(&g_CriticalSection);
	return 0;
}

int ECM_GpioIntEnable( uint8_t ch, uint8_t inttype)
{
	int i=0;
	uint8_t IdxCheck;
	EnterCriticalSection(&g_CriticalSection);
	g_SpiCmd.pHead->u8Cmd = ECM_GPIO_CONFIG_SET;
	g_SpiCmd.pHead->u8Param = 1;
	g_SpiCmd.pHead->u16Size = 0;
	g_SpiCmd.pHead->u8Idx++;
	if(ch < 20 && inttype < 6){
		g_SpiCmd.pHead->u8Data[0] = ch;
		g_SpiCmd.pHead->u8Data[1] = inttype;
	}else{
		LeaveCriticalSection(&g_CriticalSection);
		return -1;
	}
	for(i=0;i<100;i++){
		if(SpiDataExchange(&IdxCheck, 0)){
			if(g_SpiCmd.pHead->u8Idx == IdxCheck){
				LeaveCriticalSection(&g_CriticalSection);
				return 1;
			}
		}
	}
	LeaveCriticalSection(&g_CriticalSection);
	return 0;
}

int ECM_GpioIntClear( uint8_t ch)
{
	int i=0;
	uint8_t IdxCheck;
	EnterCriticalSection(&g_CriticalSection);
	g_SpiCmd.pHead->u8Cmd = ECM_GPIO_CONFIG_SET;
	g_SpiCmd.pHead->u8Param = 4;
	g_SpiCmd.pHead->u16Size = 0;
	g_SpiCmd.pHead->u8Idx++;
	if(ch < 8){
		g_SpiCmd.pHead->u8Data[0] = 1u << ch;
		g_SpiCmd.pHead->u8Data[1] = 0;
	}else if (ch < 16){
		g_SpiCmd.pHead->u8Data[1] = 1u << (ch-8);
		g_SpiCmd.pHead->u8Data[0] = 0;
	}else if(ch < 20){
        g_SpiCmd.pHead->u8Param = 6;
        g_SpiCmd.pHead->u8Data[0] = 0;
        g_SpiCmd.pHead->u8Data[1] = 0;
        g_SpiCmd.pHead->u8Data[2] = 1u << (ch-16);
    }else{
		LeaveCriticalSection(&g_CriticalSection);
		return -1;
	}
	for(i=0;i<100;i++){
		if(SpiDataExchange(&IdxCheck, 0)){
			if(g_SpiCmd.pHead->u8Idx == IdxCheck){
				LeaveCriticalSection(&g_CriticalSection);
				return 1;
			}
		}
	}
	LeaveCriticalSection(&g_CriticalSection);
	return 0;
}

int ECM_GpioGetIntFlag( uint16_t *val)
{
	int i=0;
	uint8_t IdxCheck;
	if(!val)
		return -1;
	EnterCriticalSection(&g_CriticalSection);
	g_SpiCmd.pHead->u8Cmd = ECM_GPIO_FUNC_OP;
	g_SpiCmd.pHead->u8Param = 5;
	g_SpiCmd.pHead->u16Size = 0;
	g_SpiCmd.pHead->u8Idx++;
	for(i=0;i<100;i++){
		if(SpiDataExchange(&IdxCheck, 0)){
			if(g_SpiCmd.pHead->u8Idx == IdxCheck){
				*val = g_SpiRet.Data[0] | (g_SpiRet.Data[1] << 8);
				LeaveCriticalSection(&g_CriticalSection);
				return 1;
			}
		}
	}
	LeaveCriticalSection(&g_CriticalSection);
	return 0;
}

int ECM_GpioExtGetIntFlag( uint32_t *val)
{
	int i=0;
	uint8_t IdxCheck;
	if(!val)
		return -1;
	EnterCriticalSection(&g_CriticalSection);
	g_SpiCmd.pHead->u8Cmd = ECM_GPIO_FUNC_OP;
	g_SpiCmd.pHead->u8Param = 7;
	g_SpiCmd.pHead->u16Size = 0;
	g_SpiCmd.pHead->u8Idx++;
	for(i=0;i<100;i++){
		if(SpiDataExchange(&IdxCheck, 0)){
			if(g_SpiCmd.pHead->u8Idx == IdxCheck){
				*val = g_SpiRet.Data[0] | (g_SpiRet.Data[1] << 8 | g_SpiRet.Data[2] << 16);
				LeaveCriticalSection(&g_CriticalSection);
				return 1;
			}
		}
	}
	LeaveCriticalSection(&g_CriticalSection);
	return 0;
}
